<?php
$worker= new GearmanWorker();
$worker->addServer();
$worker->addFunction("createSQL", function($job){
	echo "Задача получена: " . $job->handle() . "\n";

	$file = file_get_contents($job->workload());
	$array = explode("\n", $file);
	$arrayLength = count($array);
	$insertingValues = [];
	for($i = 0; $i < $arrayLength; $i++){
		//если пустой ряд
		if (empty($array[$i])) continue;
		$values = explode(',', $array[$i]);
		$id = $values[0];
		$orderInfo = $values[1];
		$price = $values[2];
		$insertingValues[] = "($id, '$orderInfo', '$price')";
	}
	$sql = "INSERT INTO temp1 (id, order_info, price) VALUES ".implode(',', $insertingValues).";";

	return $sql;
});

print "Ожидание задачи...\n";
while (true){
	$worker->work();
	if ($worker->returnCode() != GEARMAN_SUCCESS){
		echo "Код возврата: " . $worker->returnCode() . "\n";
		break;
	}
}

?>
