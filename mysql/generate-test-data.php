<?php
/**
 * Author: alfred
 * Email: alfred
 */

$fileName = '../data/data14.csv';
$cities = [
	'Москва', 'Санкт-Петербург', 'Пермь', 'Cевастополь', 'Мурманск', 'Екатеринбург', 'Новосибирск', 'Архангельск'
];
//$currencies = ['RUR', 'UAH', 'EUR'];
$to = rand(10000, 40000);
for($i =0; $i < $to; $i++){
	$id = rand(1, 2000);
	$offerCondition = date('d.m.Y', rand(strtotime('22-09-2008'), strtotime('30-09-2015'))) . ';' . rand(0,9) . ';AI,' . $cities[rand(0, 7)];
	$price = floor(rand(100, 5000)/10)*10 . 'RUB';
	file_put_contents(
		$fileName, $id . ',' . $offerCondition . ',' . $price . "\n", FILE_APPEND
	);
}
print 'success ' . rand(0, 1999) . ' ' . $fileName;
