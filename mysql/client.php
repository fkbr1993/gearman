<?php
$server = "localhost";
$user = "root";
$password = "mysql";
$db = "uran";

$connection = new mysqli($server, $user, $password, $db);
if ($connection->connect_error) {
	die("Не удалось установить соединение с MySQL: " . $connection->connect_error);
}

//создаём временную таблицу для всех данных
//если данные не помещаются в RAM, то можно попробовать диск, тогда просто CREATE TABLE
$sql = 'CREATE TEMPORARY TABLE temp1 (id INTEGER, order_info VARCHAR(255), price VARCHAR(255))';

if ($connection->query($sql)) {
	echo "Создана временная таблица для всех данных\n";
} else {
	echo "Ошибка: <br>" . $connection->error;
}

$client= new GearmanClient();
$client->addServer();
$client->setCompleteCallback(function($task) use ($connection){//используем одно и то же подключение
	if ($connection->query($task->data()) === TRUE) {
		echo "Записи перенесены в MySQL ".$task->unique()."\n";
	} else {
		echo "Ошибка: " . $task->data() . "<br>" . $connection->error;
	}
});

for ($i = 0; $i < 15; $i++) {
	$fileName = "../data/data".$i.".csv";
	$client->addTask("createSQL", $fileName, null, $i+1);
}

$client->runTasks();

//изящно, можно менять число входящих в топ, но слишком сложно O(n^2)
/*$sql = 'select id, order_info, price
		from `order` as o1
		where (
		   select count(*) from `order` as o2
		   where o2.id = o1.id and o2.price <= o1.price
		) <= 20;';*/

//линейная сложность через UNION

//получаем все id по одному разу
$sql = "SELECT DISTINCT id FROM temp1";

if ($connection->real_query($sql) === TRUE) {
	echo "Получены секции (id)\n";
} else {
	echo "Ошибка: " . $sql . "<br>" . $connection->error;
}

$sql = [];
$res = $connection->use_result();
while ($row = $res->fetch_assoc()) {
	//для каждого id делаем выборку 20 самых дешевых заказов
	$sql[] = ' (SELECT id, order_info, price FROM `order` WHERE id = ' . $row['id'] . ' ORDER BY price*1 LIMIT 20) ';
}

/**
 * @var string $sql
 * Вставляем по 20 самых дешевых заказов для каждого id во временную таблицу
 * Пример:
 * INSERT INTO temp2 (id, order_info, price)
 *  (SELECT id, order_info, price FROM `order` WHERE id = 181 ORDER BY price*1 LIMIT 20) UNION
 *  (SELECT id, order_info, price FROM `order` WHERE id = 11 ORDER BY price*1 LIMIT 20) UNION
 *  (SELECT id, order_info, price FROM `order` WHERE id = 42 ORDER BY price*1 LIMIT 20) UNION
 *  ...
 *  (SELECT id, order_info, price FROM `order` WHERE id = 58 ORDER BY price*1 LIMIT 20) UNION
 *  (SELECT id, order_info, price FROM `order` WHERE id = 179 ORDER BY price*1 LIMIT 20); *
 */
$sql = 'CREATE TEMPORARY TABLE IF NOT EXISTS temp2 AS ' . implode('UNION', $sql);


if ($connection->real_query($sql) === TRUE) {
	echo "Заполнена промежуточная таблица\n";
} else {
	echo "Ошибка: " . $sql . "<br>" . $connection->error;
}

//выбираем не более 1000 результатов отсортированных по price
$sql = 'SELECT id, order_info, price FROM temp2 ORDER BY price*1 LIMIT 1000;';

if ($connection->real_query($sql) === TRUE) {
	echo "Получены отсортированные данные (id)\n";
} else {
	echo "Ошибка: " . $sql . "<br>" . $connection->error;
}

$res = $connection->use_result();

//первый проход цикла без FILE_APPEND
$row = $res->fetch_assoc();
file_put_contents('../data/result.csv', $row['id'] . "," . $row['order_info'] . ',' .  $row['price'] . "\n");

while ($row = $res->fetch_assoc()) {
	file_put_contents('../data/result.csv', $row['id'] . "," . $row['order_info'] . ',' .  $row['price'] . "\n", FILE_APPEND);
}

echo "Результат сохранен в файл data/result.csv\n";
$connection->close();

?>
