# Использование алгоритма

### Шаг 1 ###
	Устанавливаем вводные данные ( файлы в формате .csv ), файлы с данными нужно поместить в папку test/data

### Шаг 2 ###
	Перечисляем какие файлы будут обработаны в файле test/map-reduce/index.php, переменная $inputFiles содержит
	массив строк, являющихся относительными путями к обрабатываемым файлам из папки test/map-reduce/, т.е. если нужно
	обработать файлы /test/data/file1.csv и /test/data/file1.csv то переменная должна содержать такой массив:
	 `$inputFiles = [
     		'../data/file1.csv',
     		'../data/file2.csv',
     ];`

### Шаг 3 ###
	Запускаем скрипт, используя следующие параметры
	 1. map - для инициализации воркера map, например test/map-reduce/index.php map
	 2. reduce - для инициализации воркера reduce, например test/map-reduce/index.php reduce
	 3. master - для инициализации master, например test/map-reduce/index.php master
	 4. show - для просмотра промежуточных результатов хранящихся в json, например test/map-reduce/index.php show intermediate-data/intermediate-data-abc.json
