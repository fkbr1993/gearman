<?php

/**
 * Диспетчер событий
 * Используется как обертка для родного диспетчера Gearman
 *
 */
class GearmanDispatcher
{

	/**
	 * @var GearmanClient
	 * Экземпляр GearmanClient используемый диспетчером
	 */
	private $client;

	/**
	 * Слушатели, хранящие call-back функции
	 * @var array
	 */
	private $listeners = [];

	/**
	 * Очередь задач, которые будут распределены между воркерами
	 * @var array
	 */
	private $dispatchQueue = [];

	/**
	 * Уникальный номер воркера
	 * @var integer
	 */
	private $unique = 0;

	/**
	 * Добавляем в список слушателей call-back
	 * @param string $event
	 * @param callable $callback
	 */
	public function addToListen($event, $callback)
	{
		$this->listeners[$event] = $callback;
	}

	/**
	 * Распределяем задачи в очереди между воркерами
	 * и запускаем их обработку
	 */
	public function dispatch()
	{
		foreach($this->dispatchQueue as $event => $params){
			if (is_array($params)){
				foreach($params as $param){
					$this->client->addTask($event, $param, null, ++$this->unique);
				}
			} else {
				$this->client->addTask($event, $params, null, ++$this->unique);
			}
		}
		$this->client->runTasks();

		// очищаем очередь
		$this->dispatchQueue = [];
	}

	/**
	 * Добавляем задачу в очередь на обработку
	 * @param string $event
	 * @param mixed $params
	 */
	public function addToDispatch($event, $params)
	{
		$this->dispatchQueue[$event][] = $params;
	}

	/**
	 * Устанавливаем экземляр GearmanClient
	 * @param GearmanClient $client
	 */
	public function setClient( GearmanClient $client){
		$this->client = $client;
	}

	/**
	 * Устанавливаем call-back функции на прослушивание
	 */
	public function listen(){
		foreach($this->listeners as $event => $callback){
			$this->client->{$event}($callback);
		}

		// очищаем список
		$this->listeners = [];
	}
}
