<?php

/**
 * Class MapReduce
 *
 * Классический map-reduce, кроме следующих особенностей:
 * 1) вводный файл с данными сразу разбит на на много файлов
 * 2) на выходе все файлы полученные после reduce сворачиваются в 1 файл
 * 3) сортирует по первому значению в строке
 *
 * Необходимые условия корректной работы данной реализации:
 * 1) вводные файлы сразу разбиты так, что два файла с промежуточными
 * данными, полученными в результате шага map,помещаются в память
 * 2) вводные данные хранятся в формате csv
 * 3) в csv файле в каждой строке три значения ( остальные будут отброшены )
 */
class MapReduce{

	/**
	 * Массив с путями к файлам с вводными данными
	 * @var array
	 */
	private $inputFiles;


	/**
	 * Функция разбивки, передаётся воркеру
	 * @var callable
	 */
	protected $map;

	/**
	 * Функция свёртки, передаётся воркеру
	 * @var callable
	 */
	protected $reduce;

	/**
	 * Путь к файлу результата
	 * @var string
	 */
	protected $result;

	/**
	 * @param array $inputFiles
	 */
	function __construct(array $inputFiles){
		$this->inputFiles = $inputFiles;
	}

	/**
	 *
	 */
	public function mapReduce(){

		// инициализируем Gearman
		$client= new GearmanClient();
		$client->addServer();

		echo "\nЗапуск... \n";

		// инициализируем диспетчер событий
		$dispatcher = new GearmanDispatcher();
		$dispatcher->setClient($client);

		// список промежуточных данных, будет заполнен на этапе разбивки (map)
		$intermediateData = [];

		// добавляем к прослушиванию callback для map воркеров
		$dispatcher->addToListen('setCompleteCallback', function($task) use (&$intermediateData){
			echo "Результат разбивки сохранен в файл: ".$task->data()."\n";
			$intermediateData[] = $task->data();
		});

		// вызываем прослушивание map воркеров
		$dispatcher->listen($intermediateData);

		// проходим по файлам с данными
		$filesCount = count($this->inputFiles);
		echo "Изначальное количество файлов для разбивки: $filesCount\n";
		for ($i = 0; $i < $filesCount; $i++) {
			$fileName = __DIR__ . DIRECTORY_SEPARATOR . $this->inputFiles[$i];

			// добавляем файл в очередь на выполнение разбивки (map)
			$dispatcher->addToDispatch("map", $fileName);
		}

		// отдаём map-задачи воркерам
		$dispatcher->dispatch();

		// получаем промежуточные данные в виде файлов созданных в результате выполнения разбивки
		$filesForReduce = $intermediateData;

		// добавляем к прослушиванию callback для reduce воркеров
		$dispatcher->addToListen('setCompleteCallback', function($task) use (&$filesForReduce){

			// добавляем результирующий файл в список на следующую итерацию сворачивания
			$filesForReduce[] = $task->data();

			// последний файл будет результатом
			$this->result = $task->data();
			echo "Результат сворачивания сохранён в файл: ".$task->data()."\n";
		});

		// вызываем прослушивание reduce воркеров
		$dispatcher->listen();

		// пока не смерджили все файлы с промежуточными результатами в 1 файл
		// первым проходом идут файлы созданные разбивкой
		// остальными проходами уже идут файлы созданные сворачиванием
		$filesForReduceCnt = count($filesForReduce);
		echo "Изначальное количество файлов для свертки: $filesForReduceCnt\n";
		while($filesForReduceCnt > 1){
			for ($i = 0; $i <= $filesForReduceCnt-1; $i+=2) {
				echo "Файлы для свертки: ".$filesForReduce[$i]." и ".(empty($filesForReduce[$i+1]) ? null : $filesForReduce[$i+1])."\n";
				$files = [$filesForReduce[$i], (empty($filesForReduce[$i+1]) ? null : $filesForReduce[$i+1]), ];
				$dispatcher->addToDispatch("reduce", json_encode($files));
			}

			// отдали файлы на обработку и очистили список
			// список снова наполнится после сворачивания, но файлов будет в 2 раза меньше
			// так продолжается пока не останется 1 файл
			$filesForReduce = [];

			// отдаём reduce-задачи воркерам
			$dispatcher->dispatch();

			// подсчитываем количество созданных файлов для следующего шага сворачивания
			$filesForReduceCnt = count($filesForReduce);
		}

		return $this->result;
	}

	/**
	 * Инициализация для воркера разбивки
	 */
	public  function workForMapper(){
		$worker= new GearmanWorker();
		$worker->addServer();
		$worker->addFunction("map", $this->getMap());

		print "Ожидание задачи...\n";
		while (true){
			$worker->work();
			if ($worker->returnCode() != GEARMAN_SUCCESS){
				echo "Код возврата: " . $worker->returnCode() . "\n";
				break;
			}
		}
	}

	/**
	 * Инициализация для воркера свертки
	 */
	public  function workForReducer(){
		$worker= new GearmanWorker();
		$worker->addServer();
		$worker->addFunction("reduce", $this->getReduce());

		print "Ожидание задачи...\n";
		while (true){
			$worker->work();
			if ($worker->returnCode() != GEARMAN_SUCCESS){
				echo "Код возврата: " . $worker->returnCode() . "\n";
				break;
			}
		}
	}

	/**
	 * Инициализирует функцию разбивки, если она не была задана,
	 * и возвращает её
	 * @return callable
	 */
	protected  function getMap(){
		if ($this->map === null){
			$this->map = function($job){

				// получаем информацию из файла
				$file = file_get_contents($job->workload());
				$array = explode("\n", $file);
				$arrayLength = count($array);
				$mappedValues = [];
				for($i = 0; $i < $arrayLength; $i++){
					if (empty($array[$i])) continue; // если пустой ряд, то пропускаем
					$values = explode(',', $array[$i]);
					$id = $values[0];
					$orderInfo = $values[1];
					$price = $values[2];
					$mappedValues[] = [$id, $orderInfo, $price];
				}

				// сохраняем промежуточные значения
				$mappedValuesImploded = json_encode($mappedValues, true);
				$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'intermediate-data'
					. DIRECTORY_SEPARATOR . 'data' . '-'.(time()-rand(0, 99999)).'.json';
				file_put_contents($filePath, $mappedValuesImploded);
				echo "Разбивка сохранена в файл [не перегрузка]: $filePath\n";

				// возвращаем адрес файла с промежуточными значениями
				return $filePath;
			};
		}
		return $this->map;
	}

	/**
	 * Инициализирует функцию свертки, если она не была задана,
	 * и возвращает её
	 * @return callable
	 */
	protected  function getReduce(){
		if ($this->reduce === null){
			$this->reduce = function($job){
				$files = json_decode($job->workload(),+ 1);
				$reducedValues = [];
				foreach($files as $file){
					if (!file_exists($file)) continue;
					$file = file_get_contents($file);
					$array = json_decode($file, true);
					foreach($array as $key=>$value){
						if (is_array($value[0])){
							$id = $key;
							foreach($value as $k=>$v){
								$orderInfo = $v[0];
								$price = $v[1];

								// ключём выставляем id, т.к. по нему сортировка будет
								$reducedValues[$id][] = [$orderInfo, $price];
							}
						} else {
							$id = $value[0];
							$orderInfo = $value[1];
							$price = $value[2];
							$reducedValues[$id][] = [$orderInfo, $price];
						}
					}
				}

				// сортируем по ключу (по id)
				ksort($reducedValues);

				// сохраняем результирующие значения
				$mappedValuesImploded = json_encode($reducedValues);
				$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'result-data'
					. DIRECTORY_SEPARATOR . 'data' . '-'.(time()-rand(0, 99999)).'.json';
				file_put_contents($filePath, $mappedValuesImploded);

				//возвращаем адрес файла с результирующими значениями
				echo "Свёртка сохранена в файл: $filePath\n";
				return $filePath;
			};
		}
		return $this->reduce;
	}
}
