#!/usr/bin/php
<?php

// если не все аргументы переданы
if($argc < 2)
{
	echo "Usage: $argv[0] map|reduce|master\n";
}
else
{
	/**
	 * Подгружатель для классов
	 * @param $class_name
	 */
	function __autoload($class_name) {
		include $class_name . '.php';
	}

	/**
	 * Выборка первых 1000 элементов из файла с отсортированными значениями
	 * Подразумевается, что файл помещается в память и его не нужно разбивать
	 *
	 * @param string $inputFileName
	 * @param string $outputFileName
	 * @param string $fileName
	 * @param integer $limit
	 */
	function finishSelection($inputFileName, $outputFileName, $limit){
		$cnt = 0;
		$fileData = json_decode(file_get_contents($inputFileName), 1);
		$resultData = '';
		foreach($fileData as $price => $values){
			foreach($values as $raw){
				$resultData .= implode(',', [$raw[0], $raw[1], $price . 'RUB']) . "\r\n";
				$cnt++;
				if ($cnt == 1000) break 2;
			}
		}
		file_put_contents($outputFileName, $resultData);
	}

	// список вводных данных
	$inputFiles = [
		'../data/data0.csv',
		'../data/data1.csv',
		'../data/data2.csv',
		'../data/data3.csv',
		'../data/data6.csv',
		'../data/data7.csv',
		'../data/data8.csv',
		'../data/data9.csv',
		'../data/data10.csv',
		'../data/data11.csv',
		'../data/data12.csv',
		'../data/data13.csv',
		'../data/data14.csv',
	];

	// инициализируем класс
	$mapReduce = new SortOrders($inputFiles);

	// переключатель от аргумента переданного в скрипт
	switch($argv[1]){
		case 'master':

			// запускаем алгоритм
			$result = $mapReduce->mapReduce();
			echo 'Результат выполнения map-reduce сохранён в файл: ' .$result . "\n";
			finishSelection($result, 'result-data/result.csv', 1000);
			echo 'Результат выполнения выборки сохранён в файл: ' . 'result-data/result.csv' . "\n";
			break;
		case 'map';

			// запускаем воркер для map
			$mapReduce->workForMapper();
			break;
		case 'reduce':

			// запускаем воркер для reduce
			$mapReduce->workForReducer();
			break;
		case 'show':

			// выводим промежуточные данные
			var_dump(json_decode(file_get_contents($argv[2]),1));
			break;
		default: echo "Usage: $argv[0] map|reduce|master\n";
	}
}
