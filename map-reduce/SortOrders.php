<?php
/**
 * Author: alfred
 * Email: alfred
 */


/**
 * Class SortOrders
 *
 * Класс реализующий модель родительский map-reduce с условиями:
 * 1) сортирует по третьему значению в строке (price)
 * 2) не более 20 вхождений для первого значения в строке (id),
 * фильтрация значений происходит после сортировки по третьему значению (price)
 */
class SortOrders extends MapReduce
{

	/** Номер значения в строке для id */
	const VALUE_ID = 0;

	/** Номер значения в строке для offer info */
	const VALUE_OFFER_INFO = 1;

	/** Номер значения в строке для price */
	const VALUE_PRICE = 2;

	/**
	 * Инициализирует функцию разбивки, если она не была задана,
	 * и возвращает её
	 *
	 * @return callable
	 */
	protected function getMap()
	{
		$this->map = function ($job) {

			// получаем информацию из файла
			$file = file_get_contents($job->workload());
			$array = explode("\n", $file);
			$arrayLength = count($array);
			$mappedValues = [];
			$selection = [];
			$count = 0;
			for ($i = 0; $i < $arrayLength; $i++) {
				if (empty($array[$i])) {
					continue;
				} // если пустой ряд, то пропускаем
				$values = explode(',', $array[$i]);
				$id = $values[self::VALUE_ID];
				$orderInfo = $values[self::VALUE_OFFER_INFO];
				$price = $values[self::VALUE_PRICE];
				$this->addToMapArray([$id, $orderInfo, $price], $mappedValues, $selection, $count);
			}

			// сохраняем промежуточные значения
			$mappedValuesImploded = json_encode($mappedValues, true);
			$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'intermediate-data'
				. DIRECTORY_SEPARATOR . 'data' . '-' . (time() - rand(0, 99999)) . '.json';
			file_put_contents($filePath, $mappedValuesImploded);
			echo "Разбивка сохранена в файл [перегрузка]: $filePath\n";

			// возвращаем адрес файла с промежуточными значениями
			return $filePath;
		};

		return $this->map;
	}

	/**
	 * Добавляем в массив, с проверкой на тот случай, если для данного id уже есть
	 * 20 записей, тогда находим запись с максимальной ценой и заменяем её на текущий элемент
	 *
	 * @param array $raw
	 * @param array $mappedValues
	 * @param array $selection
	 * @param integer $count
	 */
	private function addToMapArray(array $raw, array &$mappedValues, array &$selection, &$count)
	{
		$id = $raw[0];
		if (!array_key_exists($id, $selection)) {
			$selection[$id]['count'] = 1;
			$selection[$id]['position'][] = $count;
			$mappedValues[$count] = $raw;
		} else {
			if ($selection[$id]['count'] < 20) {
				$selection[$id]['count']++;
				$selection[$id]['position'][] = $count;
				$mappedValues[$count] = $raw;
			} else {

				// принимаем первую запись за максимально дорогую для данного id
				$position = $selection[$id]['position'][0];
				$value = 1 * $mappedValues[$position][self::VALUE_PRICE];
				$maxPrice = $value;
				$maxPosition = $position;
				for ($i = 1; $i < 20; $i++) {
					$position = $selection[$id]['position'][$i];
					$value = 1 * $mappedValues[$position][self::VALUE_PRICE];
					if ($value > $maxPrice) {
						$maxPrice = $value;
						$maxPosition = $position;
					}
				}

				// заменяем только в случае если цена текущего элемента меньше максимальной
				if ($raw[self::VALUE_PRICE] * 1 < $maxPrice) {
					$mappedValues[$maxPosition] = $raw;
				}

				// новая вставка не происходит, поэтому следующий шаг снова с тем же номером
				$count--;
			}
		}
		$count++;
	}

	/**
	 * Инициализирует функцию свертки, если она не была задана,
	 * и возвращает её
	 *
	 * @return callable
	 */
	protected function getReduce()
	{
		$this->reduce = function ($job) {
			$files = json_decode($job->workload(), +1);
			$reducedValues = [];
			$selection = [];
			foreach ($files as $file) {
				if (!file_exists($file)) {
					continue;
				}
				$file = file_get_contents($file);
				$array = json_decode($file, true);
				foreach ($array as $key => $value) {
					if (is_array($value[0])) { // если файл был создан шагом reduce
						$price = 1*$key;
						foreach ($value as $k => $v) {
							$id = $v[0];
							$orderInfo = $v[1];

							// ключём выставляем price, т.к. по нему сортировка будет
							$this->addToReduceArray($reducedValues, $price, [$id, $orderInfo, ], $selection);
						}
					} else { // если файл был создан шагом map
						$id = $value[0];
						$orderInfo = $value[1];
						$price = 1*$value[2];
						$this->addToReduceArray($reducedValues, $price, [$id, $orderInfo, ], $selection);
					}
				}
			}

			// сортируем по ключу (по price)
			ksort($reducedValues);

			// сохраняем результирующие значения
			$mappedValuesImploded = json_encode($reducedValues);
			$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'result-data'
				. DIRECTORY_SEPARATOR . 'data' . '-' . (time() - rand(0, 99999)) . '.json';
			file_put_contents($filePath, $mappedValuesImploded);

			//возвращаем адрес файла с результирующими значениями
			echo "Свёртка сохранена в файл: $filePath\n";

			return $filePath;
		};

		return $this->reduce;
	}

	/**
	 * Добавляем в массив, с проверкой на тот случай, если для данного id уже есть
	 * 20 записей, тогда находим запись с максимальной ценой и заменяем её на текущий элемент
	 *
	 * @param array $reducedValues
	 * @param integer $price
	 * @param array $raw
	 * @param array $selection
	 */
	protected function addToReduceArray(array &$reducedValues, $price, array $raw, array &$selection){
		$id = $raw[0];
		if (!array_key_exists($id, $selection)) {
			$selection[$id]['count'] = 1;
			$selection[$id]['position'][] = $price;
			$reducedValues[$price][] = $raw;
		} else {
			if ($selection[$id]['count'] < 20) {
				$selection[$id]['count']++;
				$selection[$id]['position'][] = $price;
				$reducedValues[$price][] = $raw;
			} else {
				$maxPriceAndPosition = $selection['position'][0];// первый элемент
				foreach($selection['position'] as $positionAndPrice){
					if ($positionAndPrice > $maxPriceAndPosition) $maxPriceAndPosition = $positionAndPrice;
				}
				if ($price < $maxPriceAndPosition){
					$cnt = count($reducedValues[$maxPriceAndPosition]);
					for($i =0;$i<$cnt; $i++){
						if ($reducedValues[$maxPriceAndPosition][$i][self::VALUE_ID] == $id){
							unset($reducedValues[$maxPriceAndPosition][$i]);
							array_values($reducedValues[$maxPriceAndPosition]);
							break;
						}
					}
					$reducedValues[$price][] = $raw;
				}
			}
		}
	}
}
